package org.linkedopenactors.kvmadapter;

import org.eclipse.rdf4j.model.IRI;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TestUser {
	private String id;
	private IRI actorId;
	private String realm;
	private String clientId;
	private String userName;
	private String userPassword;
}
