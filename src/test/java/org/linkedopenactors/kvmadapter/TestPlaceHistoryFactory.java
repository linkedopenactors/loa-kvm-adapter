package org.linkedopenactors.kvmadapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.util.Values;
import org.junit.jupiter.api.Test;
import org.linkedopenactors.kvmadapter.kvm.PlaceHistoryFactory;
import org.linkedopenactors.kvmadapter.kvm.PlaceRevision2PublicationLoaModel;
import org.linkedopenactors.kvmadapter.kvm.model.PlaceHistory;
import org.springframework.core.io.ClassPathResource;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.utils.ModelLogger;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class TestPlaceHistoryFactory {

	@Test
	void test() throws IOException {
		ClassPathResource cpr = new ClassPathResource("sample.json");
		String json = new BufferedReader(
			      new InputStreamReader(cpr.getInputStream(), StandardCharsets.UTF_8))
			        .lines()
			        .collect(Collectors.joining("\n"));

		PlaceHistory ph = PlaceHistoryFactory.getInstance().create(json);
		
		
		IRI subject = Values.iri("http://example.com");
		Namespace namespace = new SimpleNamespace("kvm", "http://localhost:8081/kvm#");
		
		List<ModelAndSubject> listOfMas =
				ph.getPlaceRevisionEntries().stream()
		.sorted((p1, p2) -> ((Long)p1.getPlaceRevision().getRev()).compareTo(p2.getPlaceRevision().getRev()))
		.map(placeRevisionEntry->PlaceRevision2PublicationLoaModel.getInstance().placeRevisionToModel(
				ph.getPlaceId(), 
				ph.getPlaceLicense(),
				placeRevisionEntry.getPlaceRevision(), 
				namespace, subject
				))
		.collect(Collectors.toList());
		Model all = new ModelBuilder().build();
		listOfMas.forEach(mas->all.addAll(mas.getModel()));
		
		ModelLogger.debug(log, all, "model");
	}
}
