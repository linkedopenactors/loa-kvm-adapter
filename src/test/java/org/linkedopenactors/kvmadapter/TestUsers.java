package org.linkedopenactors.kvmadapter;


import static org.eclipse.rdf4j.model.util.Values.iri;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.eclipse.rdf4j.model.IRI;

public class TestUsers {

	public static final String RDF_PUB_ADMIN_PWD = "admin";
	public static final String RDF_PUB_ADMIN = "rdf-pub-admin";
	private static final String APPLICATION_REALM = "LOA";
	private static final String APPLICATION_CLIENT_ID = "rdfpub-local";

	private KeycloakConfiguration keycloakConfiguration;
	private List<TestUser> testUsers = new ArrayList<>();
	private TestUser rdfPubAdmin = new TestUser(null, null, APPLICATION_REALM, APPLICATION_CLIENT_ID, RDF_PUB_ADMIN, RDF_PUB_ADMIN_PWD);	
	private TestUser max = new TestUser(null, null, APPLICATION_REALM, APPLICATION_CLIENT_ID, "max", "max");
	private TestUser kvmadapter = new TestUser(null, null, APPLICATION_REALM, APPLICATION_CLIENT_ID, "kvmadapter", "kvmadapter");
	
	private String rdfPubServerUrl;

	public TestUsers(KeycloakConfiguration keycloakConfiguration, String rdfPubServerUrl) {
		this.keycloakConfiguration = keycloakConfiguration;
		this.rdfPubServerUrl = rdfPubServerUrl;
		init();
	}
	
	public void init() {
		for (int i = 1; i < 21; i++) {
			UUID uuid = UUID.randomUUID();
			testUsers.add(new TestUser(null, null, APPLICATION_REALM, APPLICATION_CLIENT_ID, "testuser_"+uuid, "testuser_pwd_"+i));	
		}				
		testUsers.forEach(user -> { 
					user.setId(keycloakConfiguration.createUser(user.getUserName(), user.getUserPassword()));
					user.setActorId(getActorId(user));
					});		
		
		rdfPubAdmin.setId(keycloakConfiguration.createUser(rdfPubAdmin.getUserName(), rdfPubAdmin.getUserPassword()));
		rdfPubAdmin.setActorId(getActorId(rdfPubAdmin));		
		max.setId(keycloakConfiguration.createUser(max.getUserName(), max.getUserPassword()));
		kvmadapter.setId(keycloakConfiguration.createUser(kvmadapter.getUserName(), kvmadapter.getUserPassword()));
		max.setActorId(getActorId(max));
		keycloakConfiguration.addReadUsersRole(APPLICATION_CLIENT_ID, rdfPubAdmin.getId());
	}
	
	public TestUser getRdfPubAdmin() {
		return rdfPubAdmin;
	}

	public TestUser getMax() {
		return max;
	}
	
	public IRI getRandomActorIri() {
		return getActorId(getRandomTestUser());
	}

	private IRI getActorId(TestUser testUser) {
		return iri(rdfPubServerUrl + testUser.getUserName());
	}

	public TestUser getRandomTestUser() {
		int min = 0;
		int max = testUsers.size()-1;
		int random = (int)Math.floor(Math.random()*(max-min+1)+min);
		return testUsers.get(random);
	}
}
