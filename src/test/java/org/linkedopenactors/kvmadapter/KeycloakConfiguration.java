package org.linkedopenactors.kvmadapter;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;

import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.lang.Nullable;

import dasniko.testcontainers.keycloak.KeycloakContainer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KeycloakConfiguration {

    private static final String ADMIN_REALM = "master";
	private static final String ADMIN_CLI = "admin-cli";
	@Nullable
    private RealmResource applicationRealmResource;

	public void init(KeycloakContainer keycloakContainer, String applicationRealm) {
		Keycloak keycloakClientMasterAdminCli = buildKeycloakClientMasterAdminCli(keycloakContainer.getAuthServerUrl(),
				keycloakContainer.getAdminUsername(), keycloakContainer.getAdminPassword());
        applicationRealmResource = keycloakClientMasterAdminCli.realms().realm(applicationRealm);
	}

	public String clientSecret(String clientId) {
		List<ClientRepresentation> clients = applicationRealmResource.clients().findByClientId(clientId);
		if(clients.size()!=1) {
			throw new RuntimeException("not exact one client found for '"+clientId+"' but: " + clients.size());
		}
		return clients.get(0).getSecret();
	}
	
    public String createUser(String name, String password) {
    	return createUser(applicationRealmResource, name, password, name+"_+firstName", name+"_+lastName", name+"@example.com");
    }
    
    private String createUser(final RealmResource realm, final String name, final String password,
            final String firstName, final String lastName, final String mail) {
    	List<TestUser> existingUsers = search(realm, name);
    	if(!existingUsers.isEmpty()) {
    		if(existingUsers.size()>1) { throw new RuntimeException("more than one user for name: " + name); }
    		else {
    			return existingUsers.get(0).getId();
    		}
    	}    	
        final UserRepresentation user = new UserRepresentation();
        user.setEnabled(Boolean.TRUE);
        user.setUsername(name);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(mail);
        user.setEmailVerified(Boolean.TRUE);
        final Response createResponse = realm.users().create(user);
        final String keycloakInternalUserId = CreatedResponseUtil.getCreatedId(createResponse);        
        log.debug("user '"+name+"' created userId: " + keycloakInternalUserId);        
        
        final CredentialRepresentation credential = new CredentialRepresentation();
        credential.setTemporary(Boolean.FALSE);
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(password);        
        final UserResource createdUser = realm.users().get(keycloakInternalUserId);
        createdUser.resetPassword(credential);
        
        return keycloakInternalUserId;
    }
    
    public void addReadUsersRole(String clientId, String keycloakInternalUserId) {
    	UserResource userResource = applicationRealmResource.users().get(keycloakInternalUserId);
    	ClientRepresentation clientRep = applicationRealmResource.clients().findByClientId("realm-management").get(0);
    	RoleRepresentation clientRoleRep = applicationRealmResource.clients().get(clientRep.getId()).roles().get("view-users").toRepresentation();
    	userResource.roles().clientLevel(clientRep.getId()).add(List.of(clientRoleRep));
    }

    private Keycloak buildKeycloakClientMasterAdminCli(final String authServerUrl, final String username, final String password) {
    	log.debug("buildKeycloakClientMasterAdminCli("+authServerUrl+", "+ADMIN_REALM+", "+ADMIN_CLI+", "+username+")");
		return KeycloakBuilder.builder()
                .serverUrl(authServerUrl)
                .realm(ADMIN_REALM)
                .clientId(ADMIN_CLI)
                .username(username)
                .password(password)
                .build();
    }
    
	public List<TestUser> search(final RealmResource realm, String name) {		
		UsersResource users = realm.users();		
		List<UserRepresentation> userRepresentations = users.search(name);
		log.trace("search by name["+name+"]: ("+userRepresentations.size()+") " + userRepresentations.stream().map(ur->ur.getId()).collect(Collectors.joining( "," )));
		if(userRepresentations.isEmpty()) {
			try {				
				userRepresentations = List.of(users.get(name).toRepresentation());
				log.trace("search by id["+name+"]: ("+userRepresentations.size()+") " + userRepresentations.stream().map(ur->ur.getId()).collect(Collectors.joining( "," )));
			} catch (NotFoundException e) {
				log.debug("search by id["+name+"] -> NOT found!");
				userRepresentations = Collections.emptyList();
			}
		}
		List<TestUser> foundUsers = userRepresentations.stream()
				.map(ur -> new TestUser(ur.getId(), iri("http://notAvailable"), ADMIN_REALM, ADMIN_CLI, ur.getUsername(), "*******")).collect(Collectors.toList());
		log.trace("foundUsers: " + foundUsers);
		return foundUsers; 
	}
    
}
