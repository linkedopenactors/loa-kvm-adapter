package org.linkedopenactors.kvmadapter;

import static org.junit.Assert.assertNotNull;

import org.eclipse.rdf4j.model.IRI;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.linkedopenactors.rdfpub.client.RdfPubClient;
import org.linkedopenactors.rdfpub.client.RdfPubClientDefault;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.web.reactive.function.client.WebClient;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Testcontainers;

import dasniko.testcontainers.keycloak.KeycloakContainer;
import lombok.extern.slf4j.Slf4j;

@Testcontainers
@Slf4j
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
public class ITBase {
	private static GenericContainer rdfPubContainer;
	private static TestUsers testUsers;
	private static final String KEYCLOAK_ALIAS = "keycloak_alias";
	protected RdfPubClient rdfPubClient4Max;
	private static final String APPLICATION_REALM = "LOA";
	private static final String APPLICATION_CLIENT_ID = "rdfpub-local";
	protected static String rdfPubServerUrl;
	protected static final int RDF_PUB_SERVER_PORT = 8081;
	protected static final int KEYCLOAK_CONTAINER_PORT = 8080;
	protected static Network NETWORK = Network.newNetwork();
	private static String rdfPubContainerImage;
	private static String KEYCLOAK_BASE_URL = "http://"+KEYCLOAK_ALIAS+":" + KEYCLOAK_CONTAINER_PORT + "/auth";
	private static String keycloakAdminUserName = "admin";
	private static String keycloakAdminPassword = "admin";
	private static KeycloakConfiguration keycloakConfiguration;
	
	@Autowired
	private WebClient webClient;
	
	protected IRI actorIri = getRandomActorIri();
	
	static {
		// Optimized for local development. Pleas set the values vie environment variables in the ci pipelines
		rdfPubContainerImage = System.getProperty("image", "rdf-pub-core") + ":"
				+ System.getProperty("imageVersion", "latest");
		log.info("using image: " + rdfPubContainerImage);
	}

    protected static final KeycloakContainer KEYCLOAK_CONTAINER = new KeycloakContainer()
            .withRealmImportFile("realm-export.json")
            .withAdminUsername(keycloakAdminUserName)
            .withAdminPassword(keycloakAdminPassword)
            .withNetwork(NETWORK)
            .withNetworkAliases(KEYCLOAK_ALIAS)
            .withExposedPorts(KEYCLOAK_CONTAINER_PORT)
            .waitingFor(Wait.forHttp("/auth/admin/master/console"));

	@SuppressWarnings({ "rawtypes", "resource", "unchecked", "deprecation" })
	private static GenericContainer getRdfPubServer(String adminUser, String adminPwd) {
		if(rdfPubContainer==null) {
			rdfPubContainer= new FixedHostPortGenericContainer(rdfPubContainerImage).withFixedExposedPort(8081, 8081)
					.withNetwork(NETWORK)
					.withExposedPorts(RDF_PUB_SERVER_PORT)
					.withEnv("app.rdfRepositoryHome", "/mnt/spring/")
					.withEnv("keycloak.realm", APPLICATION_REALM)
					.withEnv("keycloak.clientid", APPLICATION_CLIENT_ID)
					.withEnv("keycloak.keycloakServerUrl", KEYCLOAK_BASE_URL)  
					.withEnv("keycloak.adminUser", adminUser)
					.withEnv("keycloak.adminPwd", adminPwd)			
					.withEnv("spring.security.oauth2.resourceserver.jwt.jwk-set-uri", KEYCLOAK_BASE_URL + "/realms/" + APPLICATION_REALM + "/protocol/openid-connect/certs")
//					.withEnv("spring_profiles_active", "debug")
					.withEnv("spring_profiles_active", "dockercompose")
//					.withLogConsumer(new Slf4jLogConsumer(log))
					.waitingFor(Wait.forHttp("/actuator/health"))
					.dependsOn(KEYCLOAK_CONTAINER);
		}
		return rdfPubContainer;
	}
	
	@BeforeAll
	public static void beforeAll() {		
		KEYCLOAK_CONTAINER.start();
		log.debug("#################################################################################");
		log.debug("                         Keycloak started");
		log.debug("#################################################################################");
		keycloakConfiguration = new KeycloakConfiguration();
		keycloakConfiguration.init(KEYCLOAK_CONTAINER, APPLICATION_REALM);		
		rdfPubContainer = getRdfPubServer(TestUsers.RDF_PUB_ADMIN, TestUsers.RDF_PUB_ADMIN_PWD);
		rdfPubContainer.start();
		rdfPubServerUrl = "http://" + rdfPubContainer.getHost() + ":" + RDF_PUB_SERVER_PORT + "/camel/";
		testUsers = new TestUsers(keycloakConfiguration, rdfPubServerUrl);
		log.debug("#################################################################################");
		log.debug("                         rdfPubContainer started");
		log.debug("#################################################################################");
	}
	
	@BeforeEach
	public void beforeEach() {
		assertNotNull(webClient);
		rdfPubClient4Max = new RdfPubClientDefault(webClient, testUsers.getMax().getActorId());
	}
	
	protected RdfPubClient getRdfPubClient(TestUser testUser) {
		return new RdfPubClientDefault(webClient, testUser.getActorId());	
	}
	
	protected TestUser getRandomTestUser() {
		return testUsers.getRandomTestUser();
	}
	
	protected IRI getRandomActorIri() {
		return testUsers.getRandomActorIri();
	}
	
	protected TestUser getMax() {
		return testUsers.getMax();
	}
}
