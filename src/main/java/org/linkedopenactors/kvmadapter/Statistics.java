package org.linkedopenactors.kvmadapter;

import lombok.Data;

@Data
public class Statistics {
	private int publications;
	private int allPlacesInclRevisions;
}
