package org.linkedopenactors.kvmadapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager;
import org.springframework.stereotype.Component;

@Component
public class AccessTokenManagerFactory {

	@Autowired
	private AuthorizedClientServiceOAuth2AuthorizedClientManager authorizedClientServiceAndManager;

	public AccessTokenManager createAccessTokenManager(String clientRegistrationId, String principalName) {
		return new AccessTokenManagerDefault(authorizedClientServiceAndManager, clientRegistrationId, principalName);
	}
}
