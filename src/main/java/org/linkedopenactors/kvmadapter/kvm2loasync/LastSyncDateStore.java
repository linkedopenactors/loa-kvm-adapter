package org.linkedopenactors.kvmadapter.kvm2loasync;

import java.io.File;
import java.time.Instant;
import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LastSyncDateStore  {

	private static final IRI lastSyncDatePredicate = Values.iri("http://linkedopenactors.ord/lastSyncDate");
	private static final IRI lastSyncDateSubject = Values.iri("http://linkedopenactors.ord/lastSyncDateSubject");
	private Repository repository;
	
	/**
	 * @param lastSyncDateStoreLocation the location in the fileSystem e.g. /mnt/spring/lastSyncDateStore 
	 */
	public LastSyncDateStore(@Value("${app.lastSyncDateStoreLocation:/mnt/spring/lastSyncDateStore}") String lastSyncDateStoreLocation) {
		repository = new SailRepository(new NativeStore(new File(lastSyncDateStoreLocation)));
	}
	
	public void lastSync(Instant lastSyncDate) {
		try(RepositoryConnection con = repository.getConnection()) {
			RepositoryResult<Statement> res = con.getStatements(lastSyncDateSubject, lastSyncDatePredicate, null);
			con.remove(res);			
			con.add(lastSyncDateSubject, lastSyncDatePredicate, Values.literal(lastSyncDate.toString()));
		}
	}
	
	public Optional<Instant> lastSyncDate() {
		try(RepositoryConnection con = repository.getConnection()) {
			RepositoryResult<Statement> res = con.getStatements(lastSyncDateSubject, lastSyncDatePredicate, null);
			
			return res.stream().findFirst()
					.map(stmt->stmt.getObject())
					.map(it->((Literal)it).temporalAccessorValue())
					.map(Instant::from);
		}
	}
}
