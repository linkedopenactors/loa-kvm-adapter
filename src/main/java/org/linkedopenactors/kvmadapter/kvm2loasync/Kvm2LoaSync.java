package org.linkedopenactors.kvmadapter.kvm2loasync;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.linkedopenactors.kvmadapter.AccessTokenManager;
import org.linkedopenactors.kvmadapter.AccessTokenManagerFactory;
import org.linkedopenactors.kvmadapter.KvmRdfPubAdapter;
import org.linkedopenactors.kvmadapter.kvm.Kvm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class Kvm2LoaSync {

	private Kvm kvm;
	private LastSyncDateStore lastSyncDateStore;
	private KvmRdfPubAdapter kvmRdfPubAdapter;

	@Value("${app.serviceAccount}")
	private String serviceAccount;
	
	@Value("${app.clientRegistrationId}") 
	private String clientRegistrationId;

	@Autowired
	private AccessTokenManagerFactory accessTokenManagerFactory;

	public Kvm2LoaSync(Kvm kvm, LastSyncDateStore lastSyncDateStore, KvmRdfPubAdapter kvmRdfPubAdapter) {
		this.kvm = kvm;
		this.lastSyncDateStore = lastSyncDateStore;
		this.kvmRdfPubAdapter = kvmRdfPubAdapter;
	}
	
	@Scheduled(fixedRate = 60000 , initialDelay = 90000)
	public void run() {
		Instant until = Instant.now();
		Instant lastSyncDate = getLastSyncDate();
		log.debug("running kvm -> loa synchronisation. Last run was: " + lastSyncDate);			
		List<String> kvmIdentifiers = kvm.getChangedEntriesSince(until, lastSyncDate).stream()
				.map(it->it.getId()).collect(Collectors.toList());
		if(!kvmIdentifiers.isEmpty()) {			
			log.debug(kvmIdentifiers.size() + " entries to change!");
			AccessTokenManager accessTokenManager = accessTokenManagerFactory.createAccessTokenManager(clientRegistrationId, serviceAccount);
			kvmRdfPubAdapter.kvmToLoaByKvmIdentifiers(kvmIdentifiers, accessTokenManager);
			log.info(kvmIdentifiers.size() + " changed entries!");
		} else {
			log.debug("No changed kvm entries.");			
		}
		lastSyncDateStore.lastSync(until);
	}

	private Instant getLastSyncDate() {
		Optional<Instant> lastSyncDateOpt = lastSyncDateStore.lastSyncDate();
		if(lastSyncDateOpt.isEmpty()) {
			lastSyncDateStore.lastSync(Instant.now());
			lastSyncDateOpt = lastSyncDateStore.lastSyncDate();
		}
		return lastSyncDateOpt.orElseThrow();
	}
}
