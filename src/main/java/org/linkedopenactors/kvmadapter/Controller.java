package org.linkedopenactors.kvmadapter;

import java.util.Collection;
import java.util.stream.Collectors;

import org.linkedopenactors.loardfpubadapter.LoaRdfPubAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class Controller {

	@Value("${app.serviceAccount}")
	private String serviceAccount;
	
	@Value("${app.clientRegistrationId}") 
	private String clientRegistrationId;

	@Autowired
	private KvmRdfPubAdapter kvmRdfPubAdapter;
	
	@Autowired
	LoaRdfPubAdapter loaRdfPubAdapter; 
	
	@Autowired
	private AccessTokenManagerFactory accessTokenManagerFactory;
	
	@RequestMapping(value = "/initialLoad", method = RequestMethod.GET)
	public ResponseEntity<?> initialLoad(@RequestParam String limit, @RequestParam(required = false) boolean force) {
		log.info("kvmRdfPubAdapter.initialLoad(limit:"+limit+")");
		long limitToUse = 0;
		if(!"ALL".equals(limit)) {
			try {
				limitToUse = Long.valueOf(limit);
			} catch (NumberFormatException e) {
				throw new IllegalStateException("You have to pass a limit as RequestParam. Use 'ALL' if there is no limit !");
			}
		}
		AccessTokenManager accessTokenManager = accessTokenManagerFactory.createAccessTokenManager(clientRegistrationId, serviceAccount);
		Collection<InitialLoadStatisticEntry> statistic = kvmRdfPubAdapter.initialLoad(limitToUse, force, accessTokenManager);
		
		Integer importedEntries 
		= statistic.stream()
				.map(it -> it.revisionCount())
				.collect(Collectors.toList()).stream()
				.mapToInt(Integer::intValue)
				.sum();
		
		StringBuilder sb = new StringBuilder();
		sb.append("<html>").append("\n");
		sb.append("<body>").append("\n");							
		
		sb.append("<h1>").append("importedEntries: " + importedEntries).append("</h1>").append("\n");
		
		statistic.forEach(statisticEntry->{
			sb.append("<ul>").append("\n");
			sb.append("<li>").append("\n");
			sb.append("kvmId: " + statisticEntry.getKvmId());
			sb.append(" ["+statisticEntry.getDurationInMilliseconds()/1000+" sec]");
			sb.append("<ul>").append("\n");
			statisticEntry.getInitialLoadStatisticRevisionEntries().values().stream().forEach(x->{
				sb.append("<li>").append("\n");
				sb.append("revision: " + x.getVersion());
				sb.append(" - " +  (x.isAvailableInLoa() ? "already existing in LOA" : "created"));
				sb.append(x.getLoaId() == null ? "" : " - " +  x.getLoaId());
				sb.append("</li>").append("\n");					
			});
			sb.append("</ul>").append("\n");
			sb.append("<ul>").append("\n");
			sb.append("</ul>").append("\n");
			sb.append("</li>").append("\n");
			sb.append("</ul>").append("\n");						
		});
		sb.append("</body>").append("\n");
		sb.append("</html>").append("\n");					

		return ResponseEntity.ok(sb.toString());
	}
	
	@RequestMapping(value = "/statistics", method = RequestMethod.GET)
	public ResponseEntity<?> statistics() {
		log.info("statistics()");

		AccessTokenManager accessTokenManager = accessTokenManagerFactory.createAccessTokenManager(clientRegistrationId, serviceAccount);
		
		int allPlacesInclRevisions = loaRdfPubAdapter.countAllPlacesInclRevisions(accessTokenManager.getToken());
	
		int publications = loaRdfPubAdapter.countPublications(accessTokenManager.getToken());
		
		StringBuilder sb = new StringBuilder();
		sb.append("<html>").append("\n");
		sb.append("<body>").append("\n");							
		
		sb.append("<h1>").append("Statistics").append("</h1>").append("\n");
		
		sb.append("<p>").append("AllPlacesInclRevisions: " + allPlacesInclRevisions).append("</p>");
		sb.append("<p>").append("Publications: " + publications).append("</p>");
		
		return ResponseEntity.ok(sb.toString());
	}
}
