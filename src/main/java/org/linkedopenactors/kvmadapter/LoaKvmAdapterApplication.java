package org.linkedopenactors.kvmadapter;

import static org.eclipse.rdf4j.model.util.Values.iri;

import org.linkedopenactors.kvmadapter.kvm.Kvm;
import org.linkedopenactors.loardfpubadapter.LoaRdfPubAdapter;
import org.linkedopenactors.rdfpub.client.RdfPubClient;
import org.linkedopenactors.rdfpub.client.RdfPubClientDefault;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.boot.actuate.trace.http.InMemoryHttpTraceRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication(exclude = { SolrAutoConfiguration.class })
@ComponentScan(basePackages = {
		"org.linkedopenactors.rdfpub.client" 
		,"org.linkedopenactors.kvmadapter" 
		,"org.linkedopenactors.kvmadapter.camel" 
		,"org.linkedopenactors.kvmadapter"
		,"org.linkedopenactors.kvmadapter.kvm2loasync"
		})
@EnableScheduling
public class LoaKvmAdapterApplication {

	@Autowired
	private WebClient webClient;
	
	@Value("${app.kvmUrl}")
	private String kvmUrl;

	@Value("${app.kvmAdminUserName}")
	private String kvmAdminUserName;

	@Value("${app.kvmAdminPassword}")
	private String kvmAdminPassword;
	
	@Value("${app.rdfpubServerBaseUrl}")
	String rdfpubServerBaseUrl;
	
	@Value("${app.serviceAccount}")
	String serviceAccount;
	
	@Value("${app.kvmNamespace}") 
	String kvmNamespace;
	
	public static void main(String[] args) {
		SpringApplication.run(LoaKvmAdapterApplication.class, args);
	}

	@Bean
	public RdfPubClient getRdfPubClient() {
		return new RdfPubClientDefault(webClient, iri(rdfpubServerBaseUrl + serviceAccount));	
	}
	
	@Bean 
	public LoaRdfPubAdapter getLoaRdfPubAdapter() {
		return new LoaRdfPubAdapter(getRdfPubClient());
	}
	
	@Bean
	public KvmRdfPubAdapter getKvmRdfPubAdapter() {
		return new KvmRdfPubAdapter(getKvm(), kvmNamespace, getLoaRdfPubAdapter(), new InitialLoadMonitor());
	}
	
	@Bean 
	public Kvm getKvm() {
		return new Kvm(kvmUrl, kvmAdminUserName, kvmAdminPassword);
	}
	
	@Bean
	public HttpTraceRepository htttpTraceRepository()
	{
	  return new InMemoryHttpTraceRepository();
	}	
}
