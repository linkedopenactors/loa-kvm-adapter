package org.linkedopenactors.kvmadapter;

import org.eclipse.rdf4j.model.IRI;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class InitialLoadStatisticRevisionEntry {
	private boolean availableInLoa = false;
	private IRI loaId;
	private long start = -1;
	private long end = -1;
	private Exception exception;
	private Long version;
	private String identifier;
	
	public InitialLoadStatisticRevisionEntry(String identifier, Long version)  {
		this.identifier = identifier;
		this.version = version;		
		start = System.currentTimeMillis();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("revision: ").append(getVersion()).append("; ");
		sb.append("loaId: ").append(getLoaId().stringValue()).append("; ");
		sb.append("alreadyInLoa: ").append(isAvailableInLoa()).append("; ");
		sb.append("durationInSeconds: ").append((end-start)/1000).append("; ");
		if(exception!=null) {
			sb.append("exception: ").append(exception.getMessage()).append("; ");
		}
		return sb.toString();
	}

	public void setError(Exception exception) {
		this.exception = exception;
	}

	public void finishPlaceRevisionProcessing() {
		end = System.currentTimeMillis();		
	}

	public void versionAlreadyExisting() {		
		setAvailableInLoa(true);		
	}	
	
	public long getDurationInMilliseconds() {
		if(start == -1 || start == -1) {
			return -1L;
		} return end - start;
	}
}