package org.linkedopenactors.kvmadapter;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.rdf4j.model.IRI;

import lombok.Data;

@Data
public class InitialLoadStatisticEntry {
	private String kvmId;
	private long start = -1;
	private long end = -1;
	private Map<Long, InitialLoadStatisticRevisionEntry> initialLoadStatisticRevisionEntries = new HashMap<>();

	public InitialLoadStatisticEntry(String kvmId) {
		this.kvmId = kvmId;
		start = java.lang.System.currentTimeMillis();
	}

	public void finishPlaceProcessing() {
		end = java.lang.System.currentTimeMillis();
	}

	public int revisionCount() {
		return initialLoadStatisticRevisionEntries.size();
	}
	
	public void add(Long version, InitialLoadStatisticRevisionEntry entry) {
		this.initialLoadStatisticRevisionEntries.put(version, entry);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("InitialLoadStatisticEntry(kvmId="+kvmId+") ["+(end-start)/1000 + " sec]\n");
		return sb.toString();
	}
	
	public void versionAlreadyExisting(Long version) {
		initialLoadStatisticRevisionEntries.get(version).versionAlreadyExisting();		
	}
	
	public void errorProcessing(Long version, Exception e) {
		initialLoadStatisticRevisionEntries.get(version).setError(e);
	}

	public void publicationCreated(Long version, IRI loaId) {
		initialLoadStatisticRevisionEntries.get(version).setLoaId(loaId);		
	}
	
	public long getDurationInMilliseconds() {
		if(start == -1 || start == -1) {
			return -1L;
		} return start-end;
	}	
}