package org.linkedopenactors.kvmadapter.kvm;

import static org.eclipse.rdf4j.model.util.Values.iri;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;

public class SubjectManagement {

	/**
	 * Takes the passed baseSubjectWitVersion and add the passed token.
	 * http://localhost:8080/test -&gt; http://localhost:8080/test_additionalToken
	 * @param baseSubjectWitVersion The context/subjet to get the IRI for incl. version.
	 * @param additionalToken The token to add to the passed baseSubjectWitVersion
	 * @return the extended IRI 
	 */
	public static IRI getSubject(IRI baseSubjectWitVersion, String additionalToken) {
		return iri(baseSubjectWitVersion.getNamespace(), baseSubjectWitVersion.getLocalName() + "_" + additionalToken);
	}

	public static IRI getBaseSubjectWithVersion(String id, Namespace namespace, String version) {
		String localname = "V" + version + "_" + id;
		IRI baseSubjectWithVersion = iri(namespace, localname);
		return baseSubjectWithVersion;
	}
	
	public static IRI getSequenceSubject(String id, Namespace namespace) {
		return iri(namespace, id);
	}

	public static IRI getPostalAddressSubject(IRI baseSubjectWitVersion) {
		return getSubject(baseSubjectWitVersion, "postalAddress");
	}
	
	public static IRI getPlaceSubject(IRI baseSubjectWitVersion) {
		return getSubject(baseSubjectWitVersion, "place");
	}

	public static IRI getContactPointSubject(IRI baseSubjectWitVersion) {
		return getSubject(baseSubjectWitVersion, "contactPoint");
	}

	public static IRI getOrganisationSubject(IRI baseSubjectWitVersion) {
		return getSubject(baseSubjectWitVersion, "organisation");
	}
}
