package org.linkedopenactors.kvmadapter.kvm.model;

import lombok.Data;

@Data
public class Contact {
	private String name = null;

	private String phone = null;

	private String email = null;

	public Contact name(String name) {
		this.name = name;
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Contact phone(String phone) {
		this.phone = phone;
		return this;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Contact email(String email) {
		this.email = email;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
