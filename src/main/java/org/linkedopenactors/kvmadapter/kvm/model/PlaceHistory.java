package org.linkedopenactors.kvmadapter.kvm.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class PlaceHistory {
	private final PlaceRoot placeRoot;
	private List<PlaceRevisionEntry> placeRevisionEntries;
	
	public void addAll(List<PlaceRevisionEntry> placeRevisionEnties) {
		if( placeRevisionEntries == null ) {
			placeRevisionEntries = new ArrayList<>();			
		}
		placeRevisionEntries.addAll(placeRevisionEnties);		
	}

	public String getPlaceId() {
		return getPlaceRoot().getId();
	}
		
	public String getPlaceLicense() {
		return getPlaceRoot().getLic();
	}
}
