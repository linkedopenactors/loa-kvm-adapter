	package org.linkedopenactors.kvmadapter.kvm.model;

import lombok.Data;

@Data
public class ReviewStatusLog {
  private Long rev = null;

  private ActivityLog act = null;

  private ReviewStatus status = null;

  public ReviewStatusLog rev(Long rev) {
    this.rev = rev;
    return this;
  }

  public Long getRev() {
    return rev;
  }

  public void setRev(Long rev) {
    this.rev = rev;
  }

  public ReviewStatusLog act(ActivityLog act) {
    this.act = act;
    return this;
  }

  public ActivityLog getAct() {
    return act;
  }

  public void setAct(ActivityLog act) {
    this.act = act;
  }

  public ReviewStatusLog status(ReviewStatus status) {
    this.status = status;
    return this;
  }

  public ReviewStatus getStatus() {
    return status;
  }

  public void setStatus(ReviewStatus status) {
    this.status = status;
  }
}
