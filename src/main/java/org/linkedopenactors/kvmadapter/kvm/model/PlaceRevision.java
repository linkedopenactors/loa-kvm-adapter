package org.linkedopenactors.kvmadapter.kvm.model;

import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaceRevision {
	private Activity created = null;

	private Long rev = null;

	private String tit = null;

	private String dsc = null;

	private Location loc = null;

	private Contact cnt = null;

	private String hrs = null;

	private LocalDate fnd = null;

	private PlaceLinks lnk = null;

	private List<String> tag = null;

	public PlaceRevision created(Activity created) {
		this.created = created;
		return this;
	}

	public Activity getCreated() {
		return created;
	}

	public void setCreated(Activity created) {
		this.created = created;
	}

	public PlaceRevision rev(Long rev) {
		this.rev = rev;
		return this;
	}

	public Long getRev() {
		return rev;
	}

	public void setRev(Long rev) {
		this.rev = rev;
	}

	public PlaceRevision tit(String tit) {
		this.tit = tit;
		return this;
	}

	public String getTit() {
		return tit;
	}

	public void setTit(String tit) {
		this.tit = tit;
	}

	public PlaceRevision dsc(String dsc) {
		this.dsc = dsc;
		return this;
	}

	public String getDsc() {
		return dsc;
	}

	public void setDsc(String dsc) {
		this.dsc = dsc;
	}

	public PlaceRevision loc(Location loc) {
		this.loc = loc;
		return this;
	}

	public Location getLoc() {
		return loc;
	}

	public void setLoc(Location loc) {
		this.loc = loc;
	}

	public PlaceRevision cnt(Contact cnt) {
		this.cnt = cnt;
		return this;
	}

	public Contact getCnt() {
		return cnt;
	}

	public void setCnt(Contact cnt) {
		this.cnt = cnt;
	}

	public PlaceRevision hrs(String hrs) {
		this.hrs = hrs;
		return this;
	}

	public String getHrs() {
		return hrs;
	}

	public void setHrs(String hrs) {
		this.hrs = hrs;
	}

	public PlaceRevision fnd(LocalDate fnd) {
		this.fnd = fnd;
		return this;
	}

	public LocalDate getFnd() {
		return fnd;
	}

	public void setFnd(LocalDate fnd) {
		this.fnd = fnd;
	}

	public PlaceRevision lnk(PlaceLinks lnk) {
		this.lnk = lnk;
		return this;
	}

	public PlaceLinks getLnk() {
		return lnk;
	}

	public void setLnk(PlaceLinks lnk) {
		this.lnk = lnk;
	}

	public PlaceRevision tag(List<String> tag) {
		this.tag = tag;
		return this;
	}

	public List<String> getTag() {
		return tag;
	}

	public void setTag(List<String> tag) {
		this.tag = tag;
	}
}
