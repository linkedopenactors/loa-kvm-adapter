package org.linkedopenactors.kvmadapter.kvm.model;

import lombok.Data;

@Data
public class Address {
	private String street = null;

	private String city = null;

	private String zip = null;

	private String country = null;

	private String state = null;

	public Address street(String street) {
		this.street = street;
		return this;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Address city(String city) {
		this.city = city;
		return this;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Address zip(String zip) {
		this.zip = zip;
		return this;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Address country(String country) {
		this.country = country;
		return this;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Address state(String state) {
		this.state = state;
		return this;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
