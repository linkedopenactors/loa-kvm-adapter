package org.linkedopenactors.kvmadapter.kvm.model;

import java.util.List;

import lombok.Data;

@Data
public class Location {
	private List<Double> deg;

	private Address adr = null;

	public Location deg(List<Double> deg) {
		this.deg = deg;
		return this;
	}

	public List<Double> getDeg() {
		return deg;
	}

	public void setDeg(List<Double> deg) {
		this.deg = deg;
	}

	public Location adr(Address adr) {
		this.adr = adr;
		return this;
	}

	public Address getAdr() {
		return adr;
	}

	public void setAdr(Address adr) {
		this.adr = adr;
	}
}
