package org.linkedopenactors.kvmadapter.kvm.model;


import java.time.LocalDate;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class KvmEntry {

	  private String title;

	  private String description;

	  private Double lat;

	  private Double lng;

	  private String street;

	  private String zip;

	  private String city;

	  private String country;

	  private String state;

	  private String contactName;

	  private String email;

	  private String telephone;

	  private String homepage;

	  private String openingHours;

	  private LocalDate foundedOn;

	  private List<String> categories;

	  private List<String> tags;

//	  private ImageUrl imageUrl = null;

//	  private ImageLink imageLinkUrl = null;

//	  private List<CustomLink> links = null;

	  private String license;

	  private String id;

	  private Integer version;

	  private Long created;

	  
//	  private List<String> ratings = null;

	
	private ReviewStatus reviewStatus;
}
