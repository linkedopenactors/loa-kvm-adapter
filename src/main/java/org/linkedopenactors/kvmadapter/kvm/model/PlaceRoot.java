package org.linkedopenactors.kvmadapter.kvm.model;

import lombok.Data;

@Data
public class PlaceRoot {
  private String id = null;

  private String lic = null;

  public PlaceRoot id(String id) {
    this.id = id;
    return this;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public PlaceRoot lic(String lic) {
    this.lic = lic;
    return this;
  }

  public String getLic() {
    return lic;
  }

  public void setLic(String lic) {
    this.lic = lic;
  }
}
