package org.linkedopenactors.kvmadapter.kvm.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PlaceRevisionEntry {
	private PlaceRevision placeRevision;
	private List<ReviewStatusLog> reviewStatusLogs;
}
