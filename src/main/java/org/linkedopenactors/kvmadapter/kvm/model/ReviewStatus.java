package org.linkedopenactors.kvmadapter.kvm.model;

public enum ReviewStatus {
  CREATED("created"),
  CONFIRMED("confirmed"),
  REJECTED("rejected"),
  ARCHIVED("archived");

  private String value;

  ReviewStatus(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
