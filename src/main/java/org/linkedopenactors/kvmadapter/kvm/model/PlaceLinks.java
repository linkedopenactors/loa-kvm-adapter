package org.linkedopenactors.kvmadapter.kvm.model;

import lombok.Data;

@Data
public class PlaceLinks {
	private String www = null;

	private String img = null;

	private String imgHref = null;

	public PlaceLinks www(String www) {
		this.www = www;
		return this;
	}

	public String getWww() {
		return www;
	}

	public void setWww(String www) {
		this.www = www;
	}

	public PlaceLinks img(String img) {
		this.img = img;
		return this;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public PlaceLinks imgHref(String imgHref) {
		this.imgHref = imgHref;
		return this;
	}

	public String getImgHref() {
		return imgHref;
	}

	public void setImgHref(String imgHref) {
		this.imgHref = imgHref;
	}
}
