package org.linkedopenactors.kvmadapter.kvm;

import static org.eclipse.rdf4j.model.util.Values.literal;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.TimeZone;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.linkedopenactors.kvmadapter.kvm.model.Address;
import org.linkedopenactors.kvmadapter.kvm.model.Contact;
import org.linkedopenactors.kvmadapter.kvm.model.PlaceRevision;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import de.naturzukunft.rdf4j.vocabulary.AS;
import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;

/**
 * A converter that knows how to convert a csv file with columns like {@link KvmCsvNames} into a LOA model.
 */
@Component
public class PlaceRevision2PublicationLoaModel {

	public static PlaceRevision2PublicationLoaModel getInstance() {
		return new PlaceRevision2PublicationLoaModel();
	}
	
	public ModelAndSubject placeRevisionToModel(
			String id, String license,  
			PlaceRevision placeRevision, Namespace namespace
			, IRI subject
			) {		
		ModelAndSubject extractPublication = extractPublication(subject, id, license, placeRevision, placeRevision.getRev());
		extractPublication.getModel().setNamespace(namespace);
		return extractPublication;
	}

	private ModelAndSubject extractPostalAddress(IRI baseSubjectWitVersion, PlaceRevision placeRevision) {
		
		IRI subject = SubjectManagement.getPostalAddressSubject(baseSubjectWitVersion);
		ModelBuilder modelBuilder = new ModelBuilder()
				.subject(subject)				
				.add(RDF.TYPE, SCHEMA_ORG.PostalAddress)
				.add(RDF.TYPE, AS.Object);
		
		if(placeRevision.getLoc()!=null && placeRevision.getLoc().getAdr()!=null) {
			Address adr = placeRevision.getLoc().getAdr();		
			Optional.ofNullable(adr.getCountry()).ifPresent(it->modelBuilder.add(SCHEMA_ORG.addressCountry, getText(it)));
			Optional.ofNullable(adr.getCity()).ifPresent(it->modelBuilder.add(SCHEMA_ORG.addressLocality, getText(it)));
			Optional.ofNullable(adr.getStreet()).ifPresent(it->modelBuilder.add(SCHEMA_ORG.streetAddress, getText(it)));
			Optional.ofNullable(adr.getZip()).ifPresent(it->modelBuilder.add(SCHEMA_ORG.postalCode, getText(it)));
		}
		return new ModelAndSubject(subject, modelBuilder.build());
	}

	private ModelAndSubject extractPlace(IRI baseSubjectWitVersion, PlaceRevision placeRevision) {
		Double latAsDouble = placeRevision.getLoc().getDeg().get(0);
		Double lngAsDouble = placeRevision.getLoc().getDeg().get(1);
		
		ModelAndSubject postalAddress = extractPostalAddress(baseSubjectWitVersion, placeRevision);
		IRI subject = SubjectManagement.getPlaceSubject(baseSubjectWitVersion);
		Model model = new ModelBuilder()
				.subject(subject)
				.add(RDF.TYPE, SCHEMA_ORG.Place)
				.add(RDF.TYPE, AS.Object)
				.add(SCHEMA_ORG.latitude, latAsDouble)
				.add(SCHEMA_ORG.longitude, lngAsDouble)
				.add(SCHEMA_ORG.address, postalAddress.getSubject())
				.build();
		model.addAll(postalAddress.getModel());
		return new ModelAndSubject(subject, model);
	}
	
	private ModelAndSubject extractContactPoint(IRI baseSubjectWitVersion, PlaceRevision placeRevision) {
		IRI subject = SubjectManagement.getContactPointSubject(baseSubjectWitVersion);
		ModelBuilder modelBuilder = new ModelBuilder()
				.subject(subject)
				.add(RDF.TYPE, SCHEMA_ORG.ContactPoint)
				.add(RDF.TYPE, AS.Object);
		Contact contact = placeRevision.getCnt();
		if(contact!=null) {
			Optional.ofNullable(contact.getEmail()).ifPresent(it->modelBuilder.add(SCHEMA_ORG.email, it));
			Optional.ofNullable(contact.getName()).ifPresent(it->modelBuilder.add(SCHEMA_ORG.name, it));
			Optional.ofNullable(contact.getPhone()).ifPresent(it->modelBuilder.add(SCHEMA_ORG.telephone, it));
		}
		return new ModelAndSubject(subject, modelBuilder.build());		
	}
	
	private ModelAndSubject extractOrgansation(IRI baseSubjectWitVersion, PlaceRevision placeRevision) {
		ModelAndSubject place = extractPlace(baseSubjectWitVersion, placeRevision);
		ModelAndSubject contactPoint = extractContactPoint(baseSubjectWitVersion, placeRevision);
		IRI subject = SubjectManagement.getOrganisationSubject(baseSubjectWitVersion);
		Model model = new ModelBuilder()
				.subject(subject)
				.add(RDF.TYPE, SCHEMA_ORG.Organization)
				.add(RDF.TYPE, AS.Object)
				.add(SCHEMA_ORG.name, placeRevision.getTit())
				.add(SCHEMA_ORG.contactPoint, contactPoint.getSubject())
				.add(SCHEMA_ORG.location, place.getSubject())
				.build();
		model.addAll(place.getModel());
		model.addAll(contactPoint.getModel());
		return new ModelAndSubject( subject, model);
	}
	
	private ModelAndSubject extractPublication(IRI baseSubjectWitVersion, String id, String license, PlaceRevision placeRevision, long version) {
		ModelAndSubject organisation = extractOrgansation(baseSubjectWitVersion, placeRevision);
		Model model = new ModelBuilder()
				.subject(baseSubjectWitVersion)
				.add(RDF.TYPE, SCHEMA_ORG.CreativeWork)
				.add(RDF.TYPE, AS.Object)
				.add(SCHEMA_ORG.name, placeRevision.getTit())
				.add(SCHEMA_ORG.creativeWorkStatus, "todo")
				.add(AS.name, placeRevision.getTit())
				.add(AS.to, AS.Public)
				.add(SCHEMA_ORG.description, placeRevision.getDsc())
				.add(SCHEMA_ORG.identifier, id)
				.add(SCHEMA_ORG.license, license)
				.add(SCHEMA_ORG.version, literal(version))
				.add(SCHEMA_ORG.about, organisation.getSubject())
				.build();
		
		model.addAll(organisation.getModel());
			
		if(placeRevision.getTag()!=null) {
			placeRevision.getTag().forEach(tag->model.add(baseSubjectWitVersion, SCHEMA_ORG.keywords, literal(tag)));
		}
		
		long created = placeRevision.getCreated().getAt();
		LocalDateTime triggerTime =
		        LocalDateTime.ofInstant(Instant.ofEpochMilli(created), 
		                                TimeZone.getDefault().toZoneId());			
		model.add(baseSubjectWitVersion, SCHEMA_ORG.dateCreated, literal(triggerTime));
		model.add(baseSubjectWitVersion, SCHEMA_ORG.dateModified, literal(LocalDateTime.now()));
		
		return new ModelAndSubject(baseSubjectWitVersion, model);
	}
	
	private String getText(String text) {
		if(StringUtils.hasText(text)) {
			return text;
		}
		return "";
	}
}
