package org.linkedopenactors.kvmadapter.kvm;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.linkedopenactors.kvmadapter.kvm.model.PlaceHistory;
import org.linkedopenactors.kvmadapter.kvm.model.PlaceRevision;
import org.linkedopenactors.kvmadapter.kvm.model.PlaceRevisionEntry;
import org.linkedopenactors.kvmadapter.kvm.model.PlaceRoot;
import org.linkedopenactors.kvmadapter.kvm.model.ReviewStatusLog;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PlaceHistoryFactory {

	private ObjectMapper objectMapper;
	
	public static PlaceHistoryFactory getInstance() {
		return new PlaceHistoryFactory();
	}
	
	public PlaceHistory create(String json) {
		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);
		objectMapper.registerModule(new JavaTimeModule());
		return convertPlaceHistory(json);
	}
	
	private PlaceHistory convertPlaceHistory(String res) {
	    try {
			JsonNode placeEntry = objectMapper.readTree(res);				
			PlaceHistory placeHistory = new PlaceHistory(
					objectMapper.readValue(placeEntry.get("place").toString(), PlaceRoot.class));
			placeHistory.addAll(
					StreamSupport.stream(placeEntry.get("revisions").spliterator(), false)
					.map(this::toPlaceRevisionEntry)
					.collect(Collectors.toList()));
			return placeHistory;	
		} catch (JsonProcessingException e) {
			log.error("kvm response body: " + res);
			throw new RuntimeException("error parsing place revisions from kvm", e);
		}
	}

	private PlaceRevisionEntry toPlaceRevisionEntry(JsonNode revisionEntry) {
		try {
			PlaceRevision placeRevision = objectMapper.readValue(revisionEntry.get(0).toString(), PlaceRevision.class);
			List<ReviewStatusLog> reviewStatusLogs = objectMapper.readValue(revisionEntry.get(1).toString(), new TypeReference<List<ReviewStatusLog>>(){});
			PlaceRevisionEntry placeRevisionEntry = new PlaceRevisionEntry(placeRevision, reviewStatusLogs);
			return placeRevisionEntry;
		} catch (JsonProcessingException e) {
			log.error("error parsing revisionEntry: " + revisionEntry.toPrettyString());
			throw new RuntimeException("error parsing revisionEntry", e);
		}
	}
}
