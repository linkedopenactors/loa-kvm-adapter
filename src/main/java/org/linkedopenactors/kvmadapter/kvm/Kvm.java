package org.linkedopenactors.kvmadapter.kvm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.linkedopenactors.kvmadapter.kvm.model.KvmEntry;
import org.linkedopenactors.kvmadapter.kvm.model.KvmSearchResult;
import org.linkedopenactors.kvmadapter.kvm.model.PlaceHistory;
import org.linkedopenactors.kvmadapter.kvm.model.PlaceRevision;
import org.linkedopenactors.kvmadapter.kvm.model.PlaceRevisionEntry;
import org.linkedopenactors.kvmadapter.kvm.model.PlaceRoot;
import org.linkedopenactors.kvmadapter.kvm.model.ReviewStatusLog;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Slf4j
@AllArgsConstructor
public class Kvm {

	private WebClient webClient;
	private String kvmUrl;
	private String kvmAdminUserName;
	private String kvmAdminPassword;
	private ObjectMapper objectMapper;
	
	public Kvm(String kvmUrl, String kvmAdminUserName, String kvmAdminPassword) {
		this.webClient = WebClient.builder().build();
		this.objectMapper = new ObjectMapper();
		this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		this.objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);
		this.objectMapper.registerModule(new JavaTimeModule());
		this.kvmUrl = kvmUrl;
		this.kvmAdminUserName = kvmAdminUserName;
		this.kvmAdminPassword = kvmAdminPassword;
	}
	
	private String getKvmToken(String userName, String pwd) {
		String url = kvmUrl + "/login";

		String body = "{\n" + "  \"email\": \"" + userName + "\",\n" + "  \"password\": \"" + pwd + "\"\n" + "}";
		return webClient.post()
				.uri(url)
				.header("accept", "application/json")
				.header("Content-Type", "application/json")
				.body(BodyInserters.fromValue(body))
				.retrieve()
				.bodyToMono(Map.class)
				.block()
				.get("token")
				.toString();
	}
	
	public PlaceHistory getPlacesHistory(String id) {
		String response = webClient.get()
									.uri(kvmUrl + "places/" + id + "/history")
									.header("Authorization", "Bearer " + getKvmToken(kvmAdminUserName, kvmAdminPassword))
									.retrieve()
									.bodyToMono(String.class)
									.block();
		
		return convertPlaceHistory(response);
	}

	public String getPlacesHistoryJson(String id) {
		String authorizationHeader = "Bearer " + getKvmToken(kvmAdminUserName, kvmAdminPassword);
		return webClient.get()
			.uri(kvmUrl + "places/" + id + "/history")
			.header("Authorization", authorizationHeader)
			.retrieve()
			.bodyToMono(String.class)
			.block();
	}

	private PlaceHistory convertPlaceHistory(String res) {
	    try {
			JsonNode placeEntry = objectMapper.readTree(res);				
			
			PlaceHistory placeHistory = new PlaceHistory(
					objectMapper.readValue(placeEntry.get("place").toString(), PlaceRoot.class));
			
			placeHistory.addAll(
					StreamSupport.stream(placeEntry.get("revisions").spliterator(), false)
					.map(this::toPlaceRevisionEntry)
					.collect(Collectors.toList()));
			
			return placeHistory;	
		} catch (JsonProcessingException e) {
			log.error("kvm response body: " + res);
			throw new RuntimeException("error parsing place revisions from kvm", e);
		}
	}

	private PlaceRevisionEntry toPlaceRevisionEntry(JsonNode revisionEntry) {
		try {
			PlaceRevision placeRevision = objectMapper.readValue(revisionEntry.get(0).toString(), PlaceRevision.class);
			List<ReviewStatusLog> reviewStatusLogs = objectMapper.readValue(revisionEntry.get(1).toString(), new TypeReference<List<ReviewStatusLog>>(){});
			PlaceRevisionEntry placeRevisionEntry = new PlaceRevisionEntry(placeRevision, reviewStatusLogs);
			return placeRevisionEntry;
		} catch (JsonProcessingException e) {
			log.error("error parsing revisionEntry: " + revisionEntry.toPrettyString());
			throw new RuntimeException("error parsing revisionEntry", e);
		}
	}
	
	public List<KvmEntry> getChangedEntriesSince(Instant until, Instant since) {
		log.debug("getting changes between " + since + " and " + until );
		long sinceAsUnixTimestamp = since.getEpochSecond();
		long untilAsUnixTimestamp = until.getEpochSecond();

		String url = kvmUrl + "entries/recently-changed?since="+sinceAsUnixTimestamp+"&until="+untilAsUnixTimestamp+"&with_ratings=false&limit=1000&offset=0";
		List<KvmEntry> response = webClient
				.get().uri(url)
				.retrieve()
				.bodyToFlux(KvmEntry.class)
				.collectList()
			    .block();

		return response;
	}

	public Optional<KvmEntry> getEntry(String id) {
		List<KvmEntry> entries = getEntry(List.of(id));
		if(entries.size()>1) {
			throw new RuntimeException("more than one entry for id: " + id);
		} else if(entries.size()==1) {
			return Optional.ofNullable(entries.get(0));
		} else {
			return Optional.empty();
		}
	}
	
	public List<KvmEntry> getEntry(List<String> ids) {
		String url = kvmUrl + "entries/"+ids.stream().collect(Collectors.joining(","));
		List<KvmEntry> response = webClient
				.get().uri(url)
				.retrieve()
				.bodyToFlux(KvmEntry.class)
				.collectList()
			    .block();

		return response;
	}

	private File getWorldEntries(long limit) {
		String urlStr = kvmUrl + "export/entries.csv?bbox=-85,-180,85,179";
		if(limit!=0) {
			urlStr += "&limit=" + limit;
		}
		try {
			File destination = File.createTempFile("kvm", "csv");
			FileOutputStream fos = new FileOutputStream(destination);
			URL url = new URL(urlStr);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setRequestProperty("Authorization", "Bearer " + getKvmToken(kvmAdminUserName, kvmAdminPassword));
			IOUtils.copy(urlConnection.getInputStream(), fos);
			System.out.println("file size: " + destination.length());
			System.out.println("destination: " + destination.getAbsolutePath());
			return destination;
		} catch (Exception e) {
			throw new RuntimeException("error downloading csv from kvm", e);
		}
	}

	public List<String> getAllEntryIds(long limit) {
		try {
			Reader in = new FileReader(getWorldEntries(limit));
			Iterable<CSVRecord> csvRecords = CSVFormat.DEFAULT.withFirstRecordAsHeader().withHeader(KvmCsvNames.class).parse(in);
			List<String> ids = StreamSupport.stream(csvRecords.spliterator(), true)
					.map(record->record.get(KvmCsvNames.id)).collect(Collectors.toList());
			log.info("csvRecords(identifiers): " + ids.size());
			return ids;
		} catch (IOException e) {
			throw new RuntimeException("error reading ids from kvm csv file", e);			
		}
	}
	
	public List<KvmEntry> findByBoundingBox(Double latleftTop, Double lngleftTop, Double latRightBottom,
			Double lngRightBottom) {

		String url = kvmUrl + "search?bbox="+latleftTop+","+lngleftTop+","+latRightBottom+","+lngRightBottom+"&status=created,confirmed";
		log.trace("kvmUrl: " + url);

		return webClient.get()
				.uri(url)
				.retrieve()
				.bodyToFlux(KvmSearchResult.class)
				.flatMap(result->Flux.fromIterable(result.getVisible()))
				.collectList()
			    .block();
	}
}
