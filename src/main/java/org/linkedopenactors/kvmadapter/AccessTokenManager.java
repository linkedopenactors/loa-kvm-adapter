package org.linkedopenactors.kvmadapter;

public interface AccessTokenManager {
	String getToken();
}