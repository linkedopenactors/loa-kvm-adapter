package org.linkedopenactors.kvmadapter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.InMemoryReactiveOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProvider;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProviderBuilder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class OAuthClientConfiguration {

	@Value("${app.clientRegistrationId}") 
	private String clientRegistrationId;
	
	@Bean
    ReactiveClientRegistrationRepository clientRegistrations( 
            @Value("${spring.security.oauth2.client.provider.loa-kvm.token-uri}") String token_uri,
            @Value("${spring.security.oauth2.client.registration.loa-kvm.client-id}") String client_id,
            @Value("${spring.security.oauth2.client.registration.loa-kvm.client-secret}") String client_secret,
            @Value("${spring.security.oauth2.client.registration.loa-kvm.scope}") String scope,
            @Value("${spring.security.oauth2.client.registration.loa-kvm.authorization-grant-type}") String authorizationGrantType
    ) {
        ClientRegistration registration = ClientRegistration
                .withRegistrationId(clientRegistrationId)
                .tokenUri(token_uri)
                .clientId(client_id)
                .clientSecret(client_secret)
                .scope(scope)
                .authorizationGrantType(new AuthorizationGrantType(authorizationGrantType))
                .build();
        return new InMemoryReactiveClientRegistrationRepository(registration);
    }

    
    @Bean
    ReactiveOAuth2AuthorizedClientService reactiveOAuth2AuthorizedClientService(ReactiveClientRegistrationRepository clientRegistrations) {
    	return new InMemoryReactiveOAuth2AuthorizedClientService(clientRegistrations);
    }
    
    @Bean
    InMemoryReactiveOAuth2AuthorizedClientService clientService(ReactiveClientRegistrationRepository clientRegistrations) {
    	return new InMemoryReactiveOAuth2AuthorizedClientService(clientRegistrations);
    }
    
    @Bean
    WebClient webClient(ReactiveClientRegistrationRepository clientRegistrations, InMemoryReactiveOAuth2AuthorizedClientService clientService) {
        AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager authorizedClientManager = new AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager(clientRegistrations, clientService);
        ServerOAuth2AuthorizedClientExchangeFilterFunction oauth = new ServerOAuth2AuthorizedClientExchangeFilterFunction(authorizedClientManager);        
		oauth.setDefaultClientRegistrationId(clientRegistrationId);
        return WebClient.builder()
                .filter(oauth)
                .build();
    }
    
    @Bean
    public AuthorizedClientServiceOAuth2AuthorizedClientManager authorizedClientServiceAndManager (
            ClientRegistrationRepository clientRegistrationRepository,
            OAuth2AuthorizedClientService authorizedClientService) {

        OAuth2AuthorizedClientProvider authorizedClientProvider =
                OAuth2AuthorizedClientProviderBuilder.builder()
                        .clientCredentials()
                        .build();

        AuthorizedClientServiceOAuth2AuthorizedClientManager authorizedClientManager =
                new AuthorizedClientServiceOAuth2AuthorizedClientManager(
                        clientRegistrationRepository, authorizedClientService);
        authorizedClientManager.setAuthorizedClientProvider(authorizedClientProvider);

        return authorizedClientManager;
    }
}
