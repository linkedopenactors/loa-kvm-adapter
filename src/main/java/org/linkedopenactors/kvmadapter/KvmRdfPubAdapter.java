package org.linkedopenactors.kvmadapter;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.util.Values;
import org.linkedopenactors.kvmadapter.kvm.Kvm;
import org.linkedopenactors.kvmadapter.kvm.PlaceHistoryFactory;
import org.linkedopenactors.kvmadapter.kvm.PlaceRevision2PublicationLoaModel;
import org.linkedopenactors.kvmadapter.kvm.model.PlaceHistory;
import org.linkedopenactors.loardfpubadapter.AlreadyExistingVersionException;
import org.linkedopenactors.loardfpubadapter.LoaRdfPubAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.utils.ModelAndSubject;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class KvmRdfPubAdapter {
	
	private Kvm kvm;
	private LoaRdfPubAdapter loaRdfPubAdapter;
	private Namespace namespace;
	private InitialLoadMonitor initialLoadMonitor;
	
	public KvmRdfPubAdapter( 
			Kvm kvm, 
			@Value("${app.kvmNamespace}") String kvmNamespace,
			LoaRdfPubAdapter loaRdfPubAdapter, InitialLoadMonitor initialLoadMonitor) {
		this.kvm = kvm;
		this.loaRdfPubAdapter = loaRdfPubAdapter;
		this.initialLoadMonitor = initialLoadMonitor;
		namespace = new SimpleNamespace("kvm", kvmNamespace);
	}
	
	public Collection<InitialLoadStatisticEntry> initialLoad(long limit, boolean force, AccessTokenManager accessTokenManager) {
		initialLoadMonitor.initialLoadStarted(force);
		kvmToLoaByKvmIdentifiers(kvm.getAllEntryIds(limit), accessTokenManager);
		initialLoadMonitor.finish();
		return initialLoadMonitor.getInitialLoadStatisticEntries();
	}
	
	public void kvmToLoaByKvmIdentifiers(List<String> kvmIdentifiers, AccessTokenManager accessTokenManager) {		
		initialLoadMonitor.placesToProcess(kvmIdentifiers.size());
		kvmIdentifiers.stream()
			.parallel()
			.forEach(kvmIdentifier -> processKvmEntry(kvmIdentifier, accessTokenManager));
	}

	private void processKvmEntry(String identifier, AccessTokenManager accessTokenManager) {
		initialLoadMonitor.startKvmEntryProcessing(identifier);
		IRI subject = Values.iri("http://example.com");
		String json = kvm.getPlacesHistoryJson(identifier);
		PlaceHistory placeHistory = PlaceHistoryFactory.getInstance().create(json);
		Set<Integer> versions = loaRdfPubAdapter.getVersions(identifier, accessTokenManager.getToken());
		try {
			placeHistory.getPlaceRevisionEntries().stream()
				.sorted((p1, p2) -> ((Long)p1.getPlaceRevision().getRev()).compareTo(p2.getPlaceRevision().getRev()))
				.filter(entry->!versions.contains( entry.getPlaceRevision().getRev().intValue()))
				.forEach(placeRevisionEntry->{
					Long revision = placeRevisionEntry.getPlaceRevision().getRev();
					ModelAndSubject placeRevisionAsModelAndSubject = PlaceRevision2PublicationLoaModel.getInstance().placeRevisionToModel(
						placeHistory.getPlaceId(), 
						placeHistory.getPlaceLicense(),
						placeRevisionEntry.getPlaceRevision(), 
						namespace, subject
						);
					processPlaceRevisionEntry(accessTokenManager, placeHistory.getPlaceId(), placeHistory.getPlaceLicense(), revision, subject, placeRevisionAsModelAndSubject);
				});
		} catch (Exception e) {
			log.error("Error processing kvm Entry " + identifier, e);
		} finally {
			initialLoadMonitor.finishKvmEntryProcessing(identifier);
		}
	}
	
	private void processPlaceRevisionEntry(AccessTokenManager accessTokenManager, String identifier, String license, Long revision, IRI subject, ModelAndSubject mas) {//PlaceRevisionEntry placeRevisionEntry) {
		Long version = Long.valueOf(revision.toString());
		initialLoadMonitor.startPlaceRevisionEntryProcessing(identifier, version);
		try {
			Instant start = Instant.now();
			IRI loaId = loaRdfPubAdapter.createPublication(mas.getModel(), accessTokenManager.getToken());
			initialLoadMonitor.publicationCreated(identifier, version, loaId, Duration.between(start, Instant.now()));
		} catch (AlreadyExistingVersionException e) {
			initialLoadMonitor.versionAlreadyExisting(identifier, version);
		} catch( Exception e) {
			log.error("error processing placeId ("+identifier+"): ", e); // todo trace
			initialLoadMonitor.errorPlaceRevisionEntryProcessing(identifier, version, e);
		} finally {			
			initialLoadMonitor.finishPlaceRevisionEntryProcessing(identifier, version);	
		}		
	}
}
