package org.linkedopenactors.kvmadapter;

import java.util.Objects;

import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.OAuth2AccessToken;

class AccessTokenManagerDefault implements AccessTokenManager {

	private AuthorizedClientServiceOAuth2AuthorizedClientManager authorizedClientServiceAndManager;
	private String clientRegistrationId;
	private String principalName;

	public AccessTokenManagerDefault(AuthorizedClientServiceOAuth2AuthorizedClientManager authorizedClientServiceAndManager, String clientRegistrationId, String principalName) {
		this.authorizedClientServiceAndManager = authorizedClientServiceAndManager;
		this.clientRegistrationId = clientRegistrationId;
		this.principalName = principalName;
	}
	
	@Override
	public String getToken() {
		OAuth2AuthorizeRequest authorizeRequest = OAuth2AuthorizeRequest.withClientRegistrationId(clientRegistrationId)
				.principal(principalName).build();
		OAuth2AuthorizedClient authorizedClient = this.authorizedClientServiceAndManager.authorize(authorizeRequest);
		OAuth2AccessToken accessToken = Objects.requireNonNull(authorizedClient).getAccessToken();
		return accessToken.getTokenValue();
	}
}
