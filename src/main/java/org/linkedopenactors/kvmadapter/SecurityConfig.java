package org.linkedopenactors.kvmadapter;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	protected void configure(final HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/actuator/**").permitAll()
			.antMatchers("/initialLoad").permitAll()// .hasRole("ADMIN")
			.antMatchers("/statistics").permitAll()// .hasRole(\"ADMIN\")
			.anyRequest().authenticated()
			.and()
			.oauth2ResourceServer().jwt();
	}
}
