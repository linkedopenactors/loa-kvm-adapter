package org.linkedopenactors.kvmadapter;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.rdf4j.model.IRI;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class InitialLoadMonitor {

	private static final int LOG_COUNTER = 50;
	private UUID currentInitialLoad = null;
	private Map<String, InitialLoadStatisticEntry> initialLoadStatisticEntries = new ConcurrentHashMap<String, InitialLoadStatisticEntry>();
	private int placesToProcess;
	private long initialLoadStarted = -1;
	private long initialLoadFinished = -1;
	private static int started = 0;
	private static int finished = 0;
	
	public UUID initialLoadStarted(boolean force) {		
		if(force) {
			currentInitialLoad=null;
		}
		if(currentInitialLoad!=null) {
			throw new IllegalStateException("Initial Load already started!");
		}
		started = 0;
		finished = 0;
		currentInitialLoad = UUID.randomUUID();
		log.info("initialLoadStarted");
		initialLoadStarted = System.currentTimeMillis();
		return currentInitialLoad;
	}
	
	public synchronized void increaseStarted() {
		started++;
	}
	
	public void startKvmEntryProcessing(String identifier) {		
		if(!log.isTraceEnabled() && !log.isDebugEnabled() && started%LOG_COUNTER==0) {
			log.info("startKvmEntryProcessing("+identifier+") [started "+started+" of "+placesToProcess+"]");
		}
		log.debug("startKvmEntryProcessing("+identifier+") [started "+started+" of "+placesToProcess+"]");
		initialLoadStatisticEntries.put(identifier, new InitialLoadStatisticEntry(identifier));
		increaseStarted();
	}
	

	public synchronized void increaseFinished() {
		finished++;
	}

	public void finishKvmEntryProcessing(String identifier) {		
		if(!initialLoadStatisticEntries.containsKey(identifier)) {
			throw new IllegalStateException("identifier: " + identifier + "  not started!");
		}
		initialLoadStatisticEntries.get(identifier).finishPlaceProcessing();
		increaseFinished();
		if(!log.isTraceEnabled() && !log.isDebugEnabled() && finished%LOG_COUNTER==0) {
			log.info("finishKvmEntryProcessing("+identifier+") [finished "+finished+" of "+placesToProcess+"]");
		}
		if(initialLoadStatisticEntries.get(identifier).revisionCount()>0) {
			log.debug("revisionCount("+identifier+"): " + initialLoadStatisticEntries.get(identifier).revisionCount());
		}
		log.trace("finishKvmEntryProcessing("+identifier+") [finished "+finished+" of "+placesToProcess+"]");		
	}

	public void startPlaceRevisionEntryProcessing(String identifier, Long version) {
		initialLoadStatisticEntries.get(identifier).add(version, new InitialLoadStatisticRevisionEntry(identifier, version));	
	}

	public void versionAlreadyExisting(String identifier, Long version) {
		log.debug("versionAlreadyExisting - " + identifier +" / v"+ version);
		initialLoadStatisticEntries.get(identifier).versionAlreadyExisting(version);
	}

	public void errorPlaceRevisionEntryProcessing(String identifier, Long version, Exception e) {
		log.error("errorProcessing (identifier: "+identifier+", version:"+version+"):", e);
		initialLoadStatisticEntries.get(identifier).errorProcessing(version, e);
	}

	public void finishPlaceRevisionEntryProcessing(String identifier, Long version) {		
		initialLoadStatisticEntries.get(identifier).finishPlaceProcessing();
	}

	public void placesToProcess(int placesToProcess) {
		log.info("placesToProcess("+placesToProcess+")");
		this.placesToProcess = placesToProcess;		
	}

	public void publicationCreated(String identifier, Long version, IRI loaId, Duration duration) {
		log.debug("publicationCreated(identifier: "+identifier+", version: "+version+", loaId: "+loaId+"), duration: " + duration.toString());
		initialLoadStatisticEntries.get(identifier).publicationCreated(version, loaId);		
	}
	
	public Collection<InitialLoadStatisticEntry> getInitialLoadStatisticEntries() {
		return initialLoadStatisticEntries.values(); 
	}

	public void finish() {
		initialLoadFinished = System.currentTimeMillis();		
		log.info("initialLoad duration: " + (initialLoadFinished-initialLoadStarted)/1000 + " sec. Processed places: " + placesToProcess);
		currentInitialLoad=null;
	}
}

