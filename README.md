This is the adapter between [kartevonmorgen](https://kartevonmorgen.org) (or more [openfairdb-api](https://openfairdb.org/) ) and a  [rdf-pub](https://rdf-pub.org) instance.

As soon as the spring boot application is started, it will regulary search for changes in the openfairdb and sync this changes to the rdf-pub instance. All Entries are imutable! Means, each change in the openfairdb is a new version of an entry and will be added.
This adapter reuses unchaged references if possible. Means, if the geo coordinates are changed but the postalAddress stays the same, ressource will be linked.

There is also an /initialLoad endpoint, that has to be called initially if a new instance of this adapter is installed. It will import all openfairdb entries into the rdf-pub instance.
