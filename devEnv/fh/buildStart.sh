#!/bin/sh
set echo on

sudo docker stop $(sudo docker ps -aq)
sudo docker rmi $(docker images -f "dangling=true" -q) --force

export SLR_BUILD_HOME=`pwd`
export WORKSPACE=/home/fredy/ws/loa-suite-ws

. ../buildApp.sh

docker-compose up 
