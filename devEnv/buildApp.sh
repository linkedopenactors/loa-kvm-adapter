#!/bin/sh
set echo on

export CURRENT_APP_HOME=$WORKSPACE/loa-kvm-adapter

cd $CURRENT_APP_HOME
echo build "loa-kvm-adapter"
mvn -q clean install -DskipTests
mkdir -p target/dependency 
cd target/dependency
jar -xf $CURRENT_APP_HOME/target/app.jar
cd ../..
docker build -t loa-kvm-adapter:latest .
cd $SLR_BUILD_HOME



